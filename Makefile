#!make
include .env
export $(shell sed 's/=.*//' .env)

.PHONY: create copy-to clear sync

create:
	mkdir -p /mnt${VOLUMES_ROOT_PATH}${PROJECT_NAME}

clear:
	docker-compose down -v

copy-to: create
	rsync -r --exclude=.git ${COPY_FILE}  /mnt${VOLUMES_ROOT_PATH}${PROJECT_NAME}/${COPY_PATH}

sync:
	for sync in $$(echo $$SYNC | tr ";" "\n"); \
	do \
		${MAKE} copy-to COPY_FILE="$${sync%>*}" COPY_PATH="$${sync#*>}"; \
	done

run-%:
	docker-compose run --rm -e MAKEFLAGS="${MAKEFLAGS}" $(shell echo ${MAKECMDGOALS} | awk '{print substr($$0, 5)}' ) ${RUN_COMMAND}

service-run-%:
	docker-compose run --rm -e MAKEFLAGS="${MAKEFLAGS}" --service-ports $(shell echo ${MAKECMDGOALS} | awk '{print substr($$0, 13)}' ) ${RUN_COMMAND}

daemon-%:
	docker-compose up -d $(shell echo ${MAKECMDGOALS} | awk '{print substr($$0, 8)}' )
