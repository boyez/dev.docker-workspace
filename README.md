# Base dockerized development workspace

Uses a docker-compose configuration and makefiles to execute task in docker container workspace

Features (Makefile targets):   
- make init: volumes (like session, ssh keys etc) init (configured with INIT_SYNC in .env file, format: src>dest;src>dest)   
- make sync: data copy from local synced/ subdirectories to container volumes (configured with SYNCED in .env file, format: src>dest;src>dest)   
- make run-%: run command in a %-matched service from the compose template (use RUN_COMMAND to override default command)    
- make make-%: run make subtarget in a %-matched service from the compose template (use MAKE_OPTIONS and MAKE_TARGET to specify make command), passes make flags to docker run command   
- make up: startups compose cluster in daemon mode    
- make clear: destroy all workspace related resource    

## Configuration
Use docker-compose.override.yml and .env file to configure your workspace features
Volumes need to be mounted in volumes service to be synced based on SYNCED env
